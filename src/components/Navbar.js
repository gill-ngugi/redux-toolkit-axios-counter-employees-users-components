import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar = () => {
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-primary navbar-expand-sm">
                <div className="container">
                    <NavLink to={"/"} className="navbar-brand">Gillian's Project</NavLink>
                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <NavLink to="/employees" className="nav-link">Employees</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/counter" className="nav-link">Counter</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/users" className="nav-link">Users</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </React.Fragment>
    );
}

export default Navbar;