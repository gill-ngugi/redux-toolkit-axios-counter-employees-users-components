import React from 'react';
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getUsers } from '../redux/features/usersSlice';

const UsersRedux = () => {
    const usersState = useSelector((state) => state.users);
    const {loading, users, errorMessage} = usersState;
    const dispatch = useDispatch();

    useEffect(async () => {
        dispatch(getUsers()); //dispatch an action 
    }, []);

    return (
        <React.Fragment>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <p className="h3 text-primary mt-2">Users </p>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        {
                            loading && <h3> Loding... </h3>
                        }
                        {
                            !loading && errorMessage && <h3 className="text-danger mt-3">{errorMessage}</h3>
                        }
                        {
                            !loading && users.length > 0 &&
                            <div>
                                <table className="table table-hover table-striped text-center">
                                    <thead className="bg-primary text-white">
                                        <tr>
                                            <th>SNO</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Website</th>
                                            <th>Company</th>
                                            <th>Location</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            users.map((user) => {
                                                return (
                                                    <tr key={user.id}>
                                                        <td>{user.id}</td>
                                                        <td>{user.name}</td>
                                                        <td>{user.email}</td>
                                                        <td>{user.website}</td>
                                                        <td>{user.company.name}</td>
                                                        <td>{user.address.city}</td>
                                                    </tr>
                                                );
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default UsersRedux;