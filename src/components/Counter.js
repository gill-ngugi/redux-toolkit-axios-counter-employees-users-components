import React from 'react';
import { useState } from 'react';

const Counter = () => {
    const [state, setState] = useState({ counter: 0 });

    const increment = () => {
        setState({counter: state.counter + 1});
    }

    const incrementByFive = () => {
        setState({counter: state.counter + 5}); 
    }

    const decrement = () => {
        setState({counter: state.counter - 1});
    }

    return (
        <React.Fragment>
            <div className="container mt-5  text-center">
                <div className="row">
                    <div className="col-md-4">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Counter</h5>
                                <p className="h3 card-text">{state.counter}</p>
                                <button className="btn btn-primary m-1" onClick={increment}>Increment</button>
                                <button className="btn btn-warning m-1" onClick={incrementByFive}>Increment by5</button>
                                <button className="btn btn-danger m-1" onClick={decrement}>Decrement</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Counter;