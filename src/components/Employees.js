import React from 'react';
import { useState } from 'react';
import { EmployeeService } from '../services/EmployeeService';

const Employees = () => {
    const [usersState, setUsersState] = useState({
        employees: EmployeeService.getEmployeeService()
    });
    const { employees } = usersState;

    const handleChecked = (id) => {
        const selectedEmployees = employees.map((employee) => {
            if (employee.id === id) {
                return {
                    ...employee,
                    isSelected: !employee.isSelected
                }
            }
            else {
                return employee;
            }
        });
        setUsersState({
            ...usersState,
            employees: selectedEmployees
        });
    }

    return (
        <React.Fragment>
            {/* <pre>{JSON.stringify(employees)}</pre> */}
            <div className="container mt-5">
                <div className="row">
                    <div className="col">
                        <p className="h3 text-primary">Employees</p>
                        <p>Lorem Ipsum Dolor</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <ul className="list-group">
                            {
                                employees.length > 0 &&
                                employees.map((employee) => {
                                    return (
                                        <li className="list-group-item" key={employee.id}>
                                            <input type="checkbox" className="form-check-imput me-2"
                                                onChange={() => handleChecked(employee.id)} />
                                            {employee.name}
                                        </li>
                                    );
                                })
                            }
                        </ul>
                    </div>
                    <div className="col">
                        {
                            employees.length > 0 &&
                            employees.map((employee) => {
                                return (
                                    <div key={employee.id}>
                                        {
                                            employee.isSelected &&
                                            <div className="card my-2">
                                                <div className="card-body">
                                                    <ul className="list-item">
                                                        <li className="list-item-group">
                                                            Name: <span>{employee.name}</span>
                                                        </li>
                                                        <li className="list-item-group">
                                                            Email: <span>{employee.email}</span>
                                                        </li>
                                                        <li className="list-item-group">
                                                            Username: <span>{employee.username}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Employees;