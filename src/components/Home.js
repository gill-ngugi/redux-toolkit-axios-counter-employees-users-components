import React from 'react';
import homePic from "../images/home-pic.jpeg"

const Home = () => {
    return (
        <React.Fragment>
            <div className="container">
                <div className="row">
                    <div className="col text-center">
                        <img src={homePic} alt="homePic" style={{width:"100%", height:"70%"}} />
                        <p className="h3 display-3 fw-bold">Gillian</p>
                        <p className="h4 display-3">Project Management</p>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Home;