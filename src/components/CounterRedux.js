import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { increment, incrementBy, decrement } from '../redux/features/counterSlice';

const CounterRedux = () => {
    const counterState = useSelector((state) => state.counter);
    const dispatch = useDispatch();

    const incrementNum = () => {
        dispatch(increment());
    }

    const incrementByNum = () => {
        dispatch(incrementBy(5));
    }

    const decrementNum = () => {
        dispatch(decrement());
    }

    return (
        <React.Fragment>
            <div className="container mt-5  text-center">
                <div className="row">
                    <div className="col-md-4">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Counter </h5>
                                <p className="h3 card-text">{counterState.count}</p>
                                <button className="btn btn-primary m-1" onClick={incrementNum}>Increment</button>
                                <button className="btn btn-warning m-1" onClick={incrementByNum}>Increment by 5</button>
                                <button className="btn btn-danger m-1" onClick={decrementNum}>Decrement</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default CounterRedux;