import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import axios from 'axios';

const Users = () => {
    const [usersState, setUsersState] = useState({
        loading: false,
        users: [],
        errorMessage: ""
    });

    const { loading, users, errorMessage } = usersState;

    useEffect(async () => {
        try {
            setUsersState({
                ...usersState,
                loading: true
            });
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            setUsersState({
                ...usersState,
                loading: false,
                users: response.data,
            });
        }
        catch (error) {
            setUsersState({
                ...usersState,
                loading: false,
                errorMessage: error
            });
        }
    }, []);

    return (
        <React.Fragment>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <p className="h3 text-primary mt-2">Users</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        {
                            loading && <h3> Loding... </h3>
                        }
                        {
                            !loading && errorMessage.length > 0 && <h3>{errorMessage}</h3>
                        }
                        {
                            !loading && users.length > 0 &&
                            <div>
                                <table className="table table-hover table-striped text-center">
                                    <thead className="bg-primary text-white">
                                        <tr>
                                            <th>SNO</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Website</th>
                                            <th>Company</th>
                                            <th>Location</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            users.map((user) => {
                                                return (
                                                    <tr key={user.id}>
                                                        <td>{user.id}</td>
                                                        <td>{user.name}</td>
                                                        <td>{user.email}</td>
                                                        <td>{user.website}</td>
                                                        <td>{user.company.name}</td>
                                                        <td>{user.address.city}</td>
                                                    </tr>
                                                );
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Users;