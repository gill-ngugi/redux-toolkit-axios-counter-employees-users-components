import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { updateChecked } from '../redux/features/employeesSlice';

const EmployeesRedux = () => {
    const employeeState = useSelector((state) => state.employees);
    const {employees} = employeeState;
    const dispatch = useDispatch();

    const handleChecked = (id) => {
        dispatch(updateChecked(id));
    }

    return (
        <React.Fragment>
            {/* <pre>{JSON.stringify(employees)}</pre> */}
            <div className="container mt-5">
                <div className="row">
                    <div className="col">
                        <p className="h3 text-primary">Employees</p>
                        <p>Lorem Ipsum Dolor</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <ul className="list-group">
                            {
                                employees.length > 0 &&
                                employees.map((employee) => {
                                    return (
                                        <li className="list-group-item" key={employee.id}>
                                            <input type="checkbox" className="form-check-imput me-2"
                                                checked={employee.isSelected}
                                                onChange={() => handleChecked(employee.id)} />
                                            {employee.name}
                                        </li>
                                    );
                                })
                            }
                        </ul>
                    </div>
                    <div className="col">
                        {
                            employees.length > 0 &&
                            employees.map((employee) => {
                                return (
                                    <div key={employee.id}>
                                        {
                                            employee.isSelected &&
                                            <div className="card my-2">
                                                <div className="card-body">
                                                    <ul className="list-group">
                                                        <li className="list-group-item">
                                                            Name: <span>{employee.name}</span>
                                                        </li>
                                                        <li className="list-group-item">
                                                            Email: <span>{employee.email}</span>
                                                        </li>
                                                        <li className="list-group-item">
                                                            Username: <span>{employee.username}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default EmployeesRedux;