import counterReducer from "./features/counterSlice";
import employeesSlice from "./features/employeesSlice";
import usersSlice from "./features/usersSlice";

const rootReducer = {
    counter: counterReducer,
    employees: employeesSlice,
    users: usersSlice
}

export default rootReducer;