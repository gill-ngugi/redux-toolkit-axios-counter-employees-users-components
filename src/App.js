import './App.css';
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import Home from './components/Home';
import Counter from './components/Counter';
import CounterRedux from './components/CounterRedux';
import Employees from './components/Employees';
import EmployeesRedux from './components/EmployeesRedux';
import Users from './components/Users';
import UsersRedux from './components/UsersRedux';

function App() {
  return (
    <React.Fragment>
      {/* <h2>THANK YOU GOD!</h2> */}
      <Router>
        <Navbar />
        <Routes>
          <Route path="/" exact element={ <Home /> } />
          <Route path="/counter" exact element={ <CounterRedux /> } />
          <Route path="/employees" exact element={ <EmployeesRedux /> } />
          <Route path="/users" exact element={ <UsersRedux /> } />
        </Routes>
      </Router>
    </React.Fragment>
  );
}

export default App;
